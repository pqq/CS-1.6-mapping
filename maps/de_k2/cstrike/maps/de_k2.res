// C:\Program Files\Counter-Strike\cstrike\maps\de_k2.res - created with RESGen v1.10
// RESGen is made by Jeroen "ShadowLord" Bogers
// URL: http://www.unitedadmins.com/mapRESGEN.asp
// E-MAIL: resgen@hltools.com
// Res creation date, GMT timezone (dd-mm-yyyy): 09-01-2003

// .res entries:
de_k2.wad
gfx/env/de_k2up.tga
gfx/env/de_k2dn.tga
gfx/env/de_k2lf.tga
gfx/env/de_k2rt.tga
gfx/env/de_k2ft.tga
gfx/env/de_k2bk.tga
models/metalgibs.mdl
models/rockgibs.mdl
models/glassgibs.mdl
models/woodgibs.mdl
models/metalplategibs.mdl
models/metalplategibs_dark.mdl
sound/de_k2/swollenmembers.wav
sound/de_k2/biosphere3.wav
sound/de_k2/biosphere4.wav
sound/de_k2/biosphere2.wav
sound/de_k2/biosphere1.wav
sound/de_k2/biosphere5.wav
sound/de_k2/biosphere6.wav
sound/de_k2/pinkf1.wav
models/computergibs.mdl
sprites/glow01.spr
